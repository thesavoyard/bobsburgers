package com.example.bobsburgers.view

import com.example.bobsburgers.model.local.Characters

data class BobsState(
    val isLoading : Boolean = false,
    val characters: List<Characters> = emptyList()
)
