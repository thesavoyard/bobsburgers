package com.example.bobsburgers.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bobsburgers.databinding.FirstFragementBinding
import com.example.bobsburgers.model.BobsRepo
import com.example.bobsburgers.view.BobsState
import com.example.bobsburgers.viewmodel.BobsVMFactory
import com.example.bobsburgers.viewmodel.BobsViewModel

class FirstFragment: Fragment() {
    private var _binding: FirstFragementBinding? = null
    private val binding get() = _binding!!
    lateinit var vmFactory: BobsVMFactory
    val viewModel by viewModels<BobsViewModel> { vmFactory }
    lateinit var bobsState: BobsState
//    val TAG = "First fragment"
    private val theAdapter: CharacterAdapter by lazy {
        CharacterAdapter(
            viewModel::updateCharacter
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FirstFragementBinding.inflate(
        inflater, container, false
    ).also {
        _binding = it
        vmFactory = BobsVMFactory(BobsRepo(requireContext()))
        initViews()
        viewModel.getCharacters()
    }.root

    fun initViews() {
        viewModel.characters.observe(viewLifecycleOwner) { state ->
            bobsState = state
            val characters = state.characters

            with(binding.rvCharacters) {
                adapter = theAdapter
                layoutManager = LinearLayoutManager(context)
            }

            theAdapter.updateList(characters)

        }

        binding.btnGetCharacters.setOnClickListener {
            viewModel.getCharacters()
        }
    }
}