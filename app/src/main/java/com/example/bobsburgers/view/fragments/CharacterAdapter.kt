package com.example.bobsburgers.view.fragments

import android.app.Activity
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Scale
import com.example.bobsburgers.R
import com.example.bobsburgers.databinding.CharacterItemBinding
import com.example.bobsburgers.model.local.Characters

class CharacterAdapter(
    val onsetFavorite: (character: Characters) -> Unit
) : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private var characters = mutableListOf<Characters>()

    fun updateList(newList: List<Characters>) {
        val oldSize = characters.size
        characters.clear()
        notifyItemRangeChanged(0, oldSize)
        characters.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder(
            CharacterItemBinding.inflate(LayoutInflater.from(parent.context)),
            onsetFavorite
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.displayCharacters(characters[position])
    }

    override fun getItemCount(): Int = characters.size

    class CharacterViewHolder(
        private val binding: CharacterItemBinding,
        val onsetFavorite: (character: Characters) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun displayCharacters(characters: Characters) {
            binding.tvName.text = characters.name
            binding.tvGender.text = characters.gender
            binding.btnFavorite.setOnClickListener {
                onsetFavorite(characters)
            }

            val screenWidth = Resources.getSystem().displayMetrics.widthPixels

            binding.ivCharacter.layoutParams.width = screenWidth - 32.toPx
            binding.ivCharacter.load(characters.image)
            binding.tvIsFavorite.text = "Favorited: ${characters.favorited}"
            binding.tvOccupation.text = characters.occupation
            binding.tvWiki.text =
                binding.root.context.getString(R.string.wiki_link, characters.wikiUrl)

            (binding.root.context as Activity).windowManager
                .defaultDisplay
                .getMetrics(DisplayMetrics());


        }
    }

}

val Int.toPx get() = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    this.toFloat(),
    Resources.getSystem().displayMetrics).toInt()