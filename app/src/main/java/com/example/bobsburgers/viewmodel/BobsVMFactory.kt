package com.example.bobsburgers.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bobsburgers.model.BobsRepo

class BobsVMFactory(
    private val repo: BobsRepo
) : ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel> create(modelClass: Class<T>) : T {
        return BobsViewModel(repo) as T
    }
}