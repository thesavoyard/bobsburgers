package com.example.bobsburgers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bobsburgers.model.BobsRepo
import com.example.bobsburgers.model.local.Characters
import com.example.bobsburgers.view.BobsState
import kotlinx.coroutines.launch

class BobsViewModel(private val repo: BobsRepo) : ViewModel() {

    private val _characters: MutableLiveData<BobsState> = MutableLiveData(BobsState())
    val characters: LiveData<BobsState> get() = _characters


    fun getCharacters() {
        viewModelScope.launch {
            _characters.value = BobsState(isLoading = true)
            val result = repo.getCharacters()
            _characters.value = BobsState(characters  = result, isLoading = false)
        }
    }

    fun updateCharacter(character: Characters) {
        viewModelScope.launch {
            val result = repo.updateCharacter(character)
        }
    }
}