package com.example.bobsburgers.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity()
data class Characters(
    @PrimaryKey
    val id:Int = 0,
    val name:String = "n/a",
    val image:String = "n/a",
    val gender:String = "n/a",
    val hairColor:String = "n/a",
    val occupation:String = "n/a",
    val firstEpisode: String = "n/a",
    val voicedBy:String = "n/a",
    val url:String = "n/a",
    val wikiUrl:String = "n/a",
    var favorited:Boolean = false
)

