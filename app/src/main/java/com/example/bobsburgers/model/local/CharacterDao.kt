package com.example.bobsburgers.model.local

import androidx.room.*

@Dao
interface CharacterDao {

    @Query("SELECT * FROM Characters")
    suspend fun getAll():List<Characters>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCharacters(vararg characters: Characters)

    @Update
    suspend fun updateCharacter(vararg characters: Characters)
}