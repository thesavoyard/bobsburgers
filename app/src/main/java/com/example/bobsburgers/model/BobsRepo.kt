package com.example.bobsburgers.model

import android.content.Context
import android.util.Log
import com.example.bobsburgers.model.local.CharacterDB
import com.example.bobsburgers.model.local.Characters
import com.example.bobsburgers.model.remote.BobsService
import com.example.bobsburgers.view.fragments.FirstFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BobsRepo(context: Context) {

    private val bobsService = BobsService.getInstance()
    private val characterDao = CharacterDB.getInstance(context).CharacterDao()
    var highest: Int = 0

    suspend fun getCharacters(): List<Characters> = withContext(Dispatchers.IO) {
        val cachedCharacters = characterDao.getAll()

        for (character in cachedCharacters) {
            if (character.id > highest) {
                highest = character.id
            }
        }
        Log.e("TAG", "highest: $highest", )
        return@withContext cachedCharacters.apply {
            if(!this.isEmpty()) //go get stuff
                Log.i("TAG", "ignore:")
            // To Do, get highest
            val range = highest+1..highest+10
            val remoteCharacters = bobsService.getCharacters(range.toList())
            Log.e("TAG", "highest: $highest", )
            characterDao.insertCharacters(*remoteCharacters.toTypedArray())

        }.ifEmpty {
            val range = 1..20
            highest = 20
            val remoteCharacters = bobsService.getCharacters(range.toList())

            characterDao.insertCharacters(*remoteCharacters.toTypedArray())
            Log.e("TAG", "highest: $highest", )
            return@withContext remoteCharacters
        }
    }

    suspend fun updateCharacter(character: Characters) = withContext(Dispatchers.IO) {
        character.favorited = true
        characterDao.updateCharacter(character)
    }
}