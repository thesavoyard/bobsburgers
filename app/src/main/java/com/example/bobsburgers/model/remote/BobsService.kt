package com.example.bobsburgers.model.remote

import androidx.room.Query
import com.example.bobsburgers.model.local.Characters
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Path

interface BobsService {

    companion object {
        private const val BASE_URL = "https://bobsburgers-api.herokuapp.com"
        private const val GET_CHARACTERS = "/characters/{Ids}"
        private const val GET_CHARACTERS_BY_IDS = "$GET_CHARACTERS{Ids}"

        fun getInstance(): BobsService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }


//    @GET("/characters/{Ids}")
    @GET(GET_CHARACTERS)
    suspend fun getCharacters(@Path("Ids") ids: List<Int>): List<Characters>
}


